import style from './style.module.css'
import LoginHeader from '../../../components/header/Auth'
import MainFooter from '../../../components/footer/MainFooter'

import { 
    Layout, 
    Row, 
    Col, 
    Form, 
    Input, 
    Button
} from 'antd'



const { Header, Content } = Layout;
const title = 'Foget password';

function App() {
    const onFinish = (values) => {
    console.log('Success:', values);
    };

    const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
    };
    
    return ( 
    <>
        <Header className={style.header} >
            <LoginHeader />
        </Header>
        <Content>
            <Row align="middle" className={style.content_position_mid}>
                <Col span={24}>
                    <Row align="center">
                        <Col>
                            <h2 className={style.pageTitle}>{title}</h2>
                        </Col>
                    </Row>
                    <Row align="middle" className={style.login_container}>
                        <Col span={24} pull={3}>
                            <Form 
                            name="basic"
                            labelCol={{
                                span: 8,
                            }}
                            wrapperCol={{
                                span: 16,
                            }}
                            initialValues={{
                                remember: true,
                            }}
                            onFinish={onFinish}
                            onFinishFailed={onFinishFailed}
                            autoComplete="off"
                            >
                                <Form.Item
                                    label="Email"
                                    name="username"
                                    rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Email!',
                                    },
                                    ]}
                                >
                                    <Input />
                                </Form.Item>

                                {/* <Form.Item
                                    label="Password"
                                    name="password"
                                    rules={[
                                    {
                                        required: true,
                                        message: 'Please input your password!',
                                    },
                                    ]}
                                >
                                    <Input.Password />
                                </Form.Item> */}

                                <Form.Item
                                    wrapperCol={{
                                    offset: 8,
                                    span: 16,
                                    }}
                                >
                                    <Button type="primary"  htmlType="submit">
                                    Send request
                                    </Button>
                                </Form.Item>
                            </Form>
                        </Col>
                    </Row>    
                </Col>
            </Row>
        </Content>
        <Layout className={style.footer}>
            <MainFooter />
        </Layout>
    </>
    );
}

export default App; 
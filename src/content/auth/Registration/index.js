import style from './style.module.css'
import { 
    Layout, 
    Row, 
    Col, 
    Form, 
    Input, 
    Button, 
    InputNumber,
    DatePicker
} from 'antd'



const { Header, Content, Footer } = Layout;
const title = 'registration';

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  };

  const validateMessages = {
    // eslint-disable-next-line no-template-curly-in-string
    required: "'${label}' is required!",
    types: {
        // eslint-disable-next-line no-template-curly-in-string
      email: "'${label} is not a valid email!'",
      // eslint-disable-next-line no-template-curly-in-string
      number: "'${label} is not a valid number!'",
    },
    number: {
        // eslint-disable-next-line no-template-curly-in-string
      range: "'${label} must be between ${min} and ${max}'",
    },
  };

function App() {
    const onFinish = (values) => {
        console.log(values);
      };
    
    return ( 
    <>
        <Header className={style.header} >
            <Row align="middle">
                <Col span={2} align="left">
                        <div className={style.logo} />   
                </Col>
            </Row>       
        </Header>
        <Content>
            <Row align="middle" className={style.content_position_mid}>
                <Col span={24}>
                    <Row align="center">
                        <Col>
                            <h2 className={style.pageTitle}>{title}</h2>
                        </Col>
                    </Row>
                    <Row align="middle" className={style.login_container}>
                        <Col span={24} pull={3}>
                            <Form {...layout} name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}>
                                <Form.Item
                                    name={['user', 'name']}
                                    label="Name"
                                    rules={[
                                    {
                                        required: true,
                                    },
                                    ]}
                                >
                                    <Input />
                                </Form.Item>
                                <Form.Item
                                    name={['user', 'email']}
                                    label="Email"
                                    rules={[
                                    {
                                        type: 'email',
                                        required: true,
                                    },
                                    ]}
                                >
                                    <Input />
                                </Form.Item>
                                <Form.Item
                                    name={['user', 'age']}
                                    label="Age"
                                    rules={[
                                    {
                                        type: 'number',
                                        min: 0,
                                        max: 99,
                                        required: true
                                    },
                                    ]}
                                >
                                    <InputNumber />                                    
                                </Form.Item>
                                <Form.Item name={['user', 'children']} label="children"  rules={[{required: true}]}>
                                    <InputNumber defaultValue="0"/>
                                </Form.Item>
                                <Form.Item name={['user', 'Cycle']} label="Last cycle" rules={[{required: true}]}>
                                    <DatePicker style={{ width: '70%' }} />
                                </Form.Item>
                                <Form.Item name={['user', 'introduction']} label="Introduction">
                                    <Input.TextArea showCount maxLength={1000} placeholder="Tell somthing about your self and goals"/>
                                </Form.Item>
                                <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                                    <Button type="primary" htmlType="submit">
                                    Submit
                                    </Button>
                                </Form.Item>
                            </Form>
                        </Col>
                    </Row>    
                </Col>
            </Row>
        </Content>
            
        <Layout className={style.footer}>
            <Footer align="center" style={{background: 'transparent'}}>Ant Design ©2018 Created by Ant UED Test task from "Pawa" by Denis Rudik</Footer>
        </Layout>
    </>
    );
}

export default App; 
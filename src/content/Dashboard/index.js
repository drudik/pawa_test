import style from './style.module.css'
import { Layout, Row, Col } from 'antd';

import HeaderDash from '../../components/header/Dash';
import MainFooter from '../../components/footer/MainFooter';
import LeftSideBar from '../../components/LeftSideBar';
import Section1 from '../../components/mainSections/Section1';
import Section2 from '../../components/mainSections/Section2';
import Section3 from '../../components/mainSections/Section3';
import Section4 from '../../components/mainSections/Section4';

const { Content, Sider } = Layout;



function App() {
    return (
        <>
        <Layout className={style.main_layout}>
            <Sider  width={300}
            style={{
                overflow: 'auto',
                height: '100vh',
                position: 'fixed',
                left: 0,
                padding: '15px 15px',
                background: 'transparent',
            }}
            >   
                <LeftSideBar />
            </Sider>
            <Layout className={style.main_layout} style={{ marginLeft: 300 }}>
                <HeaderDash />
            <Content style={{ padding: '15px', overflow: 'initial',  }}>
                <div>
                    <Row gutter={[16,16]}>
                        <Col span={12}>
                            <Section1 />
                        </Col>
                        <Col span={12}>
                            <Section2 />
                        </Col>
                        <Col span={12}>
                            <Section3 />  
                        </Col>
                        <Col span={12}>
                            <Section4 />
                        </Col>
                    </Row>
                </div>
            </Content>
                <MainFooter className={style.footer}/>
            </Layout>
        </Layout>
        </>
    );
}

export default App;
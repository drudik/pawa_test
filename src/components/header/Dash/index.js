import style from './style.module.css'
import { Link } from 'react-router-dom';
import { Col, Row, Image, Space } from 'antd';
import { LoginOutlined, SettingOutlined } from '@ant-design/icons'

import userPhoto from './user_photo.jpg'

const title = "dashboard";


function App() {
    return (
        <Row className={style.header_wrapper} align="middle">
            <Col flex="auto" align="center">
                <h2 className={style.pageTitle}>{title}</h2>
            </Col>
            <Col flex="none">
                <Space>
                    <Image width={50} className={style.userImage} preview={false} src={userPhoto} /> 
                    <SettingOutlined className={style.settingOutlined} />
                    <Link className={style.logout_btn} to={'/login'}>
                        <LoginOutlined className={style.loginOutlined}/>   
                    </Link>
                </Space>
            </Col>
        </Row>       
    );
}

export default App;
import style from './style.module.css'

import { Col, Row } from 'antd';



function App() {
    return (
        <Row align="middle">
            <Col span={2} align="left">
                    <div className={style.logo} />   
            </Col>
        </Row>      
    );
}

export default App;
import { Layout } from 'antd'

const { Footer } = Layout;

function App() {
    return (
        <>
            <Footer align="center" style={{background: 'transparent'}}>Ant Design ©2018 Created by Ant UED Test task from "Pawa" by Denis Rudik</Footer>
        </>
    );
}

export default App;
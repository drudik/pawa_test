import style from './style.module.css'
import { Row, Col, Calendar } from 'antd';

const title = 'calendar'; 
function App() {
    function onPanelChange(value, mode) {
        console.log(value, mode);
      }
    
    return (
        <>
            <Row className={[style.title_style].join(' ')} align="center">
                <Col>
                    <h2>{title}</h2>
                </Col>
            </Row>   
            <div className={[style.grid_container_frame, style.test].join(' ')}>
                <div className="site-calendar-demo-card">
                    <Calendar fullscreen={false} onPanelChange={onPanelChange} />
                </div>
            </div>
        </>
    );
}

export default App;
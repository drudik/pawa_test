import style from './style.module.css';
// import { useState } from 'react';
import { Row, Col, Transfer, Button } from 'antd';

const title = 'goals';

const getMock = () => {
    const mockData = [];
    const targetKeys = [];

    for (let i = 0; i < 20; i++) {
      const data = {
        key: i.toString(),
        title: `content${i + 1}`,
        description: `description of content${i + 1}`,
        chosen: Math.random() * 2 > 1,
      };
    //   if (data.chosen) {
    //     targetKeys.push(data.key);
    //   }
      return mockData.push(data);
    }
    // this.setState({ mockData, targetKeys });
  };


const renderFooter = (props, { direction }) => {
    if (direction === 'left') {
      return (
        <Button size="small" style={{ float: 'left', margin: 5 }}>
          Left button reload
        </Button>
      );
    }
    return (
      <Button size="small" style={{ float: 'right', margin: 5 }}>
        Right button reload
      </Button>
    );
}


function App() {
    // const [mockData, setMockData] = useState(' ');
    // const [targetKeys, setTargetKeys] = useState(' ');
    
    return (
        <>
            <Row className={[style.title_style].join(' ')} align="center">
                <Col>
                    <h2>{title}</h2>
                </Col>
            </Row>   
            <div className={[style.grid_container_frame, style.test].join(' ')}>
            <Transfer
                // dataSource={this.state.mockData}
                showSearch
                listStyle={{
                width: "100%",
                height: 400,
                }}
                // operations={['to right', 'to left']}
                // targetKeys={this.state.targetKeys}
                // onChange={this.handleChange}
                // render={item => `${item.title}-${item.description}`}
                footer={renderFooter}
            />
            </div>
        </>
    );
}

export default App;






  

  

  

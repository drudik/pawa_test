import { Progress } from 'antd';

function App() {
    return (
        <div>
            <p>some items</p>
            <Progress percent={30} size="small" />
            <p>some items</p>
            <Progress percent={50} size="small" status="active" />
            <p>some items</p>
            <Progress percent={70} size="small" status="exception" />
            <p>some items</p>
            <Progress percent={100} size="small" />
            <p>some items</p>
            <Progress percent={70} size="small" status="exception" />
            <p>some items</p>
            <Progress percent={100} size="small" />
        </div>
        
    );
}

export default App;
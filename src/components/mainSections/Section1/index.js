import style from './style.module.css'
import { Row, Col } from 'antd';

import DataPicker from './DataPicker';
import TimeLine from './TImeLine';
import ProgressBarGorisontal from './ProgressBarGorisontal';

const title = 'progress'; 


function App() {
    return (
        <>
            <Row className={[style.title_style].join(' ')} align="center">
                <Col>
                    <h2>{title}</h2>
                </Col>
            </Row>   
            <div className={[style.grid_container_frame, style.test].join(' ')}>
                <Row gutter={[16, 16]}>
                    <Col>
                       <DataPicker /> 
                    </Col>
                </Row>
                <br />
                <Row gutter={[16, 16]}>
                    <Col span={12}>
                        <TimeLine />  
                    </Col>
                    <Col span={12}>
                        <ProgressBarGorisontal />
                    </Col>
                </Row>
            </div>
        </>
    );
}

export default App;
import style from './style.module.css'
import { Row, Col, Carousel } from 'antd';

const title = 'your log'; 

const contentStyle = {
    height: '400px',
    color: '#fff',
    lineHeight: '160px',
    textAlign: 'center',
    background: '#364d79',
  };
  
function App() {
    return (
        <>
            <Row className={[style.title_style].join(' ')} align="center">
                <Col>
                    <h2>{title}</h2>
                </Col>
            </Row>   
            <div className={[style.grid_container_frame, style.test].join(' ')}>
            <Carousel autoplay>
                <div>
                <h3 style={contentStyle}>Place some content here</h3>
                </div>
                <div>
                <h3 style={contentStyle}>and here</h3>
                </div>
                <div>
                <h3 style={contentStyle}>and here again</h3>
                </div>
                <div>
                <h3 style={contentStyle}>ofcourse you can place here too</h3>
                </div>
            </Carousel>
            </div>
        </>
    );
}

export default App;




// import style from './style.module.css'
import { Progress, Row, Col } from 'antd';

function App() {
    return (
     <>
        <Row gutter={[16,16]} align="center">
            <Col align="center">
                <p>Cycles</p>
                <Progress type="circle" percent={30} width={80} />
            </Col>
            <Col align="center">
                <p>Work</p>
                <Progress type="circle" percent={50} width={80} status="exception" />
            </Col>
            <Col align="center">
                <p>Rest</p>
                <Progress type="circle" percent={100} width={80} />
            </Col>
            <Col align="center">
                <p>Food</p>
                <Progress type="circle" percent={100} width={80} />
            </Col>
        </Row>
     </>
    );
}

export default App;
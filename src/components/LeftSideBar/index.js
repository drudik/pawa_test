import style from './style.module.css'
import { Row, Col } from 'antd';

import Menu from './Menu';
import ProgressBar from './ProgressBar';
import LastPost from './LastPost';

const mainTitle = 'menu';
const statisticTiele = 'statistic';
const storyTitle = 'last story';

function App() {
    return (
        <>
            <div className={style.logo} />
            <Row className={[style.title_style, style.title_margin].join(' ')} align="center">
                <Col>
                    <h2>{mainTitle}</h2>
                </Col>
            </Row>   
            <div className={style.sider_container_frame}>
                <Row>
                    <Col span={24}>
                        <Menu />
                    </Col>
                </Row>
                <br />
                <Row className={style.title_style} align="center">
                    <Col>
                        <h2>{statisticTiele}</h2>
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                        <ProgressBar />
                    </Col>
                </Row>
                <br />
                <Row className={style.title_style} align="center">
                    <Col>
                        <h2>{storyTitle}</h2>
                    </Col>
                </Row>
                <Row align="center">
                    <Col span={22}>
                       <LastPost /> 
                    </Col>
                </Row>
            </div>
        
        </>
        
    );
}

export default App;
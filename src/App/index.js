// import style from './style.module.css';
import { useState } from 'react';
import { Routes, Route } from 'react-router-dom';

import Login from '../content/auth/Login';
import Registration from '../content/auth/Registration';
import ResetPwd from '../content/auth/ResetPwd';
import FogetPwd from '../content/auth/FogetPwd';
import Dashboard from '../content/Dashboard';


function App() {
  const [token, setToken] = useState(' ');
  
  if (!token) {
    return <Login setToken={setToken} />
  }

  return (
    <>
      <Routes>
        <Route path='/' />
        <Route path="/login" element={<Login />} />
        <Route path="/registration" element={<Registration />} />
        <Route path="/reset-password" element={<ResetPwd />} />
        <Route path="/foget-password" element={<FogetPwd />} />
        <Route path="/dashboard" element={<Dashboard />} />
        <Route path="*" element={"404"} />
      </Routes>
    </>
  );
}

export default App;
